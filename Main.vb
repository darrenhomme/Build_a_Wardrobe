'Build a Wardrobe 1.1

Dim ShowTotal as Boolean                              'Animate total VP
Dim LeftVpPennies as Array[Integer]                   'VPs in pennies, treat as range by default, low side
Dim RightVpPennies as Array[Integer]                  'VPs in pennies, treat as range by default, high side
Dim LeftPricePennies as Array[Integer]                'Price in pennies, treat as range by default, low side
Dim RightPricePennies as Array[Integer]               'Price in pennies, treat as range by default, high side
Dim Devider as Array[String]                          'Keep track of / - or none for ranges

'Build arrays
For i = 1 to 4
    LeftVpPennies.Push(0)
    RightVpPennies.Push(0)
    LeftPricePennies.Push(0)
    RightPricePennies.Push(0)
	Devider.Push("")
Next

'Calc Image size, VPs, and total VPs on start
Sub OnInit()
     For i = 1 to 4
          ImageSize(i)
          ValuePay()
     Next
End Sub

'Subs to pass value from external image script
Sub ResizeImage1()
     ImageSize(1)
End Sub

Sub ResizeImage2()
     ImageSize(2)
End Sub

Sub ResizeImage3()
     ImageSize(3)
End Sub

Sub ResizeImage4()
     ImageSize(4)
End Sub

Sub ImageSize(ImageNumber as Integer)
   'Get Image Texture and container IDs
   Dim ImageContianer as String
   Dim ImageTexture as String
   ImageContianer = CStr(Scene.FindContainer("Item_" & ImageNumber).FindSubContainer("Image").FindSubContainer("Image"))  
   ImageTexture = CStr(System.SendCommand("0 #" & ImageContianer & "*TEXTURE GET 0;"))

   'Get Texture Size
   Dim SizeText as String
   SizeText = System.SendCommand("0 " & ImageTexture & "*INFO GET 0;")

   'Get Image Ratio
   dim Size_Split as Array[String]
   SizeText.split(" ",Size_Split)  
   Dim RatioText as String
   RatioText = CStr(CDbl(Size_Split[0]) / CDbl(Size_Split[1]))
   RatioText = RatioText.Left(3)
   'this.geometry.text = RatioText


   'Set Ratio, with container locked only X or Y is needed
   If CDbl(RatioText) >= 1 then RatioText = "0.8"
   System.SendCommand("0 #" & ImageContianer & "*TRANSFORMATION*SCALING*Y SET " & RatioText & ";")
End Sub

Sub NumberOfItems()
     'Find number of items from user input
     Dim NumberItems as String
     NumberItems = Cstr(Scene.FindContainer("Number_of_Items").geometry.text)

    'Position items, 500 hides unused off screen
     If NumberItems = "1" then
          SetItemX(1, 0)
          SetItemX(2, 500)
          SetItemX(3, 500)
          SetItemX(4, 500)
     End If

     If NumberItems = "2" then
          SetItemX(1, -130)
          SetItemX(2, 130)
          SetItemX(3, 500)
          SetItemX(4, 500)
     End If

     If NumberItems = "3" then
          SetItemX(1, -190)
          SetItemX(2, 0)
          SetItemX(3, 190)
          SetItemX(4, 500)
     End If

     If NumberItems = "4" then
          SetItemX(1, -260)
          SetItemX(2, -87)
          SetItemX(3, 87)
          SetItemX(4, 260)
     End If
End Sub

Sub SetItemX(Number as Integer, Value as Integer)
     Scene.FindContainer("Item_" & CStr(Number)).position.x = Value
End Sub

Sub ValuePay()
     Dim LeftSide as Integer
	 Dim RightSide as Integer
	 Dim MiddleDevider as String
	 
     'Get user input for number of items on screen
     Dim NumberItems as Integer
     NumberItems = CInt(Scene.FindContainer("Number_of_Items").geometry.text)
	 
     'Get prices from user input and calc VPs
	 For i = 1 to NumberItems
          GetAmount(i, "Price")
		  SetVp(i)
     Next

     For i = 1 to NumberItems
          GetAmount(i, "VP")
     Next	 
	 
     'Check for price range
     LeftSide = AddRange(NumberItems, "VP", "Left")
     RightSide = AddRange(NumberItems, "VP", "Right")	

     'Defualt / for two price range, change to - if larger range is detected
     MiddleDevider = "/"
     For i = 1 to NumberItems
          If Devider[i-1] = "-" then MiddleDevider = "-"
     Next
	 
     'If range then use devider
     If LeftSide <> RightSide then
          TotalVP(ToDollars(LeftSide) & " " & MiddleDevider & " " & ToDollars(RightSide))
     Else
          TotalVP(ToDollars(LeftSide))
     End If
 
     'Show bar if any item has VP
 	 Dim CountVpforBar as Integer
     For i = 1 to NumberItems
          'VpOverOneShow gets VP number and hides individual VP
	      CountVpforBar = CountVpforBar + VpOverOneShow(i)
	 Next
	 
	 If CountVpforBar > NumberItems then
          ShowStripe(1)
     Else
          ShowStripe(0)
     End if

     Dim AllOverOneVpCheck as Boolean
     AllOverOneVpCheck = True
     For i = 1 to NumberItems
          If GetVp(i) < 2 then AllOverOneVpCheck = False
     Next

     'Hides VP total if one item doesn't have VP
     If AllOverOneVpCheck = False then
	      ShowTotal = False
	 Else
	      ShowTotal = True
	 End If
	 
	 ShowTotalSetting()
End Sub

'Get VP and price and look for ranges
Sub GetAmount(ItemNumber as Integer, AmountType as String)
     Dim Amount as String
     If AmountType = "VP" then Amount = Cstr(Scene.FindContainer("Item_" & ItemNumber).FindSubContainer("VP_Price").geometry.text)   
	 If AmountType = "Price" then Amount = Cstr(Scene.FindContainer("Item_" & ItemNumber).FindSubContainer("Price").geometry.text)
	 
     Range(ItemNumber, Amount, AmountType)
End Sub

'Split at / or -, duplicate price if no range. Send ranges to be converted to pennies
Sub Range(ItemNumber as Integer, Amount as String, AmountType as String)
     Dim AmountSplit as Array[String]
     If Amount.find("/") > -1 or Amount.find("-") > -1 then
          If Amount.find("/") > -1 then
		       Amount.split("/",AmountSplit)
			   Devider[ItemNumber-1] = "/"
          End If
		  
          If Amount.find("-") > -1 then
		       Amount.split("-",AmountSplit)
			   Devider[ItemNumber-1] = "-"
		  End If
     Else
          AmountSplit.Push(Amount)
		  AmountSplit.Push(Amount)
		  Devider[ItemNumber-1] = ""
     End If
		  
     If AmountType = "VP" then
	      LeftVpPennies[ItemNumber-1] = ToPennies(AmountSplit[0])
          RightVpPennies[ItemNumber-1] = ToPennies(AmountSplit[1])
	 End If
	 
     If AmountType = "Price" then
          LeftPricePennies[ItemNumber-1] = ToPennies(AmountSplit[0])
          RightPricePennies[ItemNumber-1] = ToPennies(AmountSplit[1])
     End If	 

End Sub

'Add up total prices
Function AddRange(Items as Integer, AmountType as String, Side as String) as Integer
     Dim Total as Integer
     For i = 0 to Items - 1
          If AmountType = "VP" and Side = "Left" then Total = Total + LeftVpPennies[i]
		  If AmountType = "VP" and Side = "Right" then Total = Total + RightVpPennies[i]
		  
          If AmountType = "Price" and Side = "Left" then Total = Total + LeftPricePennies[i]
		  If AmountType = "Price" and Side = "Right" then Total = Total + RightPricePennies[i]
	 Next
	 AddRange = Total
End Function

'Remove $, multiply dollars by 100 and add cents if present
Function ToPennies(Amount as String) as Integer
     Amount.Trim
     If Amount.find("$") > -1 then
          Dim AmountSplit as Array[String]
          Amount.split("$",AmountSplit)

          If AmountSplit[0] <> "" then
               Amount = AmountSplit[0]
          Else
               Amount = AmountSplit[1]
          End If
     End If

     Amount.Trim
     If Amount.find(".") > -1 then
          Dim CentsSplit as Array[String]
          Amount.split(".",CentsSplit)
          If CentsSplit[1].Length = 1 then CentsSplit[1] = CentsSplit[1] & "0"
          Amount = CStr( (CInt(CentsSplit[0]) * 100) + CInt(CentsSplit[1]))
     Else
           Amount = CStr(CInt(Amount) * 100)
     End if

	 ToPennies = CInt(Amount)
End Function

'convert pennies back to dollars
Function ToDollars(Pennies as Integer) as String
     Dim Dollars as String
     Dim PenniesString as String
     PenniesString = CStr(Pennies)
	 
     If Pennies > 99 then
          Dollars = PenniesString.Left(PenniesString.Length - 2) & "." & PenniesString.Right(2)
     Else
          Dollars = "0." & CStr(Pennies)
     End If

     ToDollars = "$" & Dollars
End Function

'Hide or show VP text if an item has VPs, Returns number of VPs
Function VpOverOneShow(ItemVpToCheck as Integer) as Integer
     Dim VpAmount as Integer
     VpAmount = CInt(Scene.FindContainer("Item_" & ItemVpToCheck).FindSubContainer("VP").geometry.text)

     Dim VpContianer as String
     VpContianer = CStr(Scene.FindContainer("Item_" & ItemVpToCheck).FindSubContainer("VP_Alpha"))
     
     If VpAmount > 1 then
          System.SendCommand("0 #" & VpContianer & "*ALPHA*ALPHA SET 100;")
     Else
          VpAmount = 1
          System.SendCommand("0 #" & VpContianer & "*ALPHA*ALPHA SET 0;")
     End If

     VpOverOneShow = VpAmount
End Function

'Just Get VP
Function GetVp(ItemVpToCalc as Integer) as Integer
     Dim VpAmount as Integer
     VpAmount = CInt(Scene.FindContainer("Item_" & ItemVpToCalc).FindSubContainer("VP").geometry.text)

     GetVp = VpAmount
End Function

'Devide Price by VP, Set VP price, use devider if it's range
Sub SetVp(ItemNumber as Integer)
     Dim ItemVp as Integer
     ItemVp = GetVp(ItemNumber)
     If ItemVp < 1 then ItemVp = 1
	 
	 Dim LeftSide as Integer
	 Dim RightSide as Integer
	 Dim MiddleDevider as String
     LeftSide = LeftPricePennies[ItemNumber - 1]
     RightSide = RightPricePennies[ItemNumber - 1]
	 MiddleDevider = Devider[ItemNumber - 1]
	 
	 LeftSide = LeftSide / ItemVp
	 RightSide = RightSide / ItemVp
	 
     If LeftSide <> RightSide then
          Scene.FindContainer("Item_" & ItemNumber).FindSubContainer("VP_Price").geometry.text = ToDollars(LeftSide) & " " & MiddleDevider & " " & ToDollars(RightSide)
     Else
          Scene.FindContainer("Item_" & ItemNumber).FindSubContainer("VP_Price").geometry.text = ToDollars(LeftSide)
     End If
End Sub

'Format total VP text
Sub TotalVP(Amount as String)
     Scene.FindContainer("Total_VP_Text").geometry.text = "Total ValuePay" & Chr(174) & " = " & Amount
End Sub

'Show or hide the bar behind the VP text based on if any item has VPs, also shifts the items up and down
Sub ShowStripe(OneOrZero as Integer)    
     If OneOrZero > 0 then
          SetAlpha("Stripe_Alpha", 100)
          SetAlpha("Total_VP_Alpha", 100)
          For i = 1 to 4
               SetItemY(i, 0)
          Next
          SetHrY("Top", 141)
          SetHrY("Bottom", -60)
     Else
          SetAlpha("Stripe_Alpha", 0)
          SetAlpha("Total_VP_Alpha", 0)
          For i = 1 to 4
               SetItemY(i, -35)
          Next
          SetHrY("Top", 106)
          SetHrY("Bottom", -95)
     End If
End Sub

'Find a container and set alpha value
Sub SetAlpha(Container as String, AlphaValue as Integer)
     Container = CStr(Scene.FindContainer(Container))
     System.SendCommand("0 #" & Container & "*ALPHA*ALPHA SET " & CStr(AlphaValue) & ";")
End Sub

'Reposition item by Y value
Sub SetItemY(Number as Integer, Value as Integer)
     Scene.FindContainer("Item_" & CStr(Number)).position.y = Value
End Sub

'Reposition the horizonatal rules by Y value
Sub SetHrY(HrName as String, Value as Integer)
     Scene.FindContainer("HR_" & HrName).position.y = Value
End Sub

'Allow or disallow playing of total animation
Sub ShowTotalSetting()
     If ShowTotal = True then
	      stage.finddirector("Default").continueanimation()
     Else
	      stage.finddirector("Default").startanimation()	 
	 End If
End Sub

